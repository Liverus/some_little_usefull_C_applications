/*
*	CrackMe N°1
*	Dyrk 2017
*
*/
#include <stdlib.h>
/*
*
*	Afficher un caractère
*/
void put_char(int c){
        write(1, &c, 1);
}
/*
*	Afficher une chaine de caractère
*
*/
void put_str(char *str){
        int i = 0;
        while (str[i] != 0){
                put_char(str[i++]);
        }
}
/*
*
*	Récupérer la taille d'une chaine de caractère
*/
int str_len(char *str){
        int i = 0;
        while (str[i] != 0) i++;
        return i;
}
/*
*
*	Vérifier si 2 chaines de caractère sont identiques
*/
int str_cmp(char *str1, char *str2){
        int i = 0;
        while (str1[i] != 0 && str2[i] != 0) {
                if (str1[i] != str2[i]){
                        return -1;
                }
                i++;
        }
        return 1;
}

/*
*
*	Convertir un Char[] en  char*
*
*/
char *copy_str(char str[]){
        char *newStr = malloc(sizeof(char) * str_len(str));
        int i = 0;
        while (str[i]  != 0)
                newStr[i] = str[i++];
        newStr[i] = 0;
        return newStr;
}

/*
*
*	Générer un nombre aléatoire entre Min et Max
*/
int random_int(int min, int max){
        return  min + rand() % (max+1 - min);
}

/*
*
*	Crackme - c'est là que vous devrez trouver le code
*	Attention celui-ci change tout le temps
*
*/
int crackme(){
        char upassword[12], *password, *upass;
        int i = 0, length_pass;
        password  = malloc(sizeof(char) * 12);
        while(i <= 10){
                password[i++] = random_int('A','Z');
        }
        password[i] = 0;
        put_str("Serial : ");
        length_pass = read(0, upassword, 12);
        upassword[length_pass] = 0;
        upass = copy_str(upassword);
        if (str_cmp(upass, password) == 1){
                put_str("Good Serial\n");
                return 1;
        }
        put_str("Bad Serial\n");
        return -1;
}
/*
*	
*	Main Code
*
*/
void main(){
        crackme();
}
