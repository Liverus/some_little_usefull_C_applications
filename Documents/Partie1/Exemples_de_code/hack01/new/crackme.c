/*
* CrackMe N°1
* Auteur original : Dy rk 2017 (programme initial)
* Source : https://dy rk.org/2017/01/30/  reverse-engineering-cracker-des-programmes-avec-gdb/
*
* Réécriture importante, création d'un script pour automatiser la compilation,
* correction des bugs et mise en forme :
* Eric Bachard // 08/10/2023
*
* Correction des bugs :
* - le programme initial ne fonctionne pas :
*    quelle que soit la chaîne de caractères entrée,
*    le mot de passe est toujours trouvé;
*    le mot de passe à trouver est toujours le même.
* Trop de warnings :
* - il ne compile pas si -Werror est utilisé
* - put_str demande un const char * et non un char *;
* - main doit retourner une valeur;
* - comportement indéfini de newStr[i] = str[i++];
* Modification de la présentation (indentation, autre charte d'écriture du code)
* Ajouts :
* - création et utilisation de la fonction initialize_rand()
* - création d'un script permettant d'automatiser la compilation
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>  // srand

// décommenter cette ligne pour vérifier le fonctionnement de ce programme
//#define VALIDATION_TEST

void
initialize_rand(void)
{
    /* on declare une variable nommee t1 de type temps (time_t)*/
    time_t t1;

    /*  time (&t1) ecrit la valeur du temps au moment de l'execution
        a l'adresse de t1
     */
    (void)srand(time(&t1));

    /*
        srand genere une suite de nombres aleatoires 
        a partir de la "graine" (valeur de t1)
        ecrite a l'adresse de t1
     */
    srand((long)t1);
}

/*
*
* Afficher un caractère
*/
void put_char(int c)
{
    write(1, &c, 1);
}

/*
* Afficher une chaine de caractère
*
*/
void put_str(const char *str)
{
    int i = 0;
    while (str[i] != 0)
    {
        put_char(str[i++]);
    }
}
/*
*
* Récupérer la taille d'une chaine de caractère
*/
int str_len(char *str)
{
    int i = 0;

    while (str[i] != 0)
        i++;

    return i;
}
/*
*
* Vérifier si 2 chaines de caractère sont identiques
* retourne vrai ssi les 2 chaînes sont identiques
*/
bool str_cmp(char *str1, char *str2)
{
    int i = 0;

    while (str1[i] != 0 && str2[i] != 0)
    {
        if (str1[i] != str2[i])
        {
            return false;
        }
        i++;
    }
    return true;
}

/*
*
* Convertir un Char[] en  char*
*
*/
char *copy_str(char str[])
{
    int i = 0;
    char *newStr = malloc(sizeof(char) * str_len(str));

    while (str[i]  != 0)
    {
        newStr[i] = str[i];
        i++;
    }

    newStr[i] = 0;

    return newStr;
}

/*
*
* Générer un nombre aléatoire entre Min et Max
*/
int random_int(int min, int max)
{
    return  min + rand() % (max + 1 - min);
}

/*
*
* Crackme - c'est là que vous devrez trouver le code
* Attention celui-ci change tout le temps
*
*/
int crackme()
{
    int    i = 0;
    int    length_pass = 0;

    char   upassword[12];
    char * password;
    char * upass;

    password  = malloc(sizeof(char) * 12);

    while(i <= 10)
    {
        password[i++] = random_int('A','Z');
    }

    password[i] = 0;

#ifdef VALIDATION_TEST
    fprintf(stdout, "password contient : %s\n", password);
#endif

    put_str("Serial : ");

    do
    {
        // read() renvoie -1 s'il échoue
        length_pass = read(0, upassword, 12);
    } while (-1 == length_pass);

    upassword[length_pass] = 0;
    upass = copy_str(upassword);

    //str_cmp retourne 1 si les chaînes sont identiques
    if (str_cmp(upass, password))
    {
        put_str("Good Serial\n");
        return 1;
    }

    put_str("Bad Serial\n");
    return -1;
}

/*
*
* Main Code
*
*/
int main(void)
{
    initialize_rand();
    crackme();
    return EXIT_SUCCESS;
}
